extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()

var jump_counter = 0
var running_right = false
var running_left = false

func _ready():
	pass

func get_input():
	velocity.x = 0
	var running_right = false
	var running_left = false
	if is_on_floor():
		jump_counter = 0
	if Input.is_action_just_pressed("up") and jump_counter < 1:
		if is_on_floor():
			velocity.y = jump_speed
		else:
			velocity.y = jump_speed
			jump_counter = 1
	if Input.is_action_pressed('right'):
		velocity.x += speed
		running_right = true
	if running_right and Input.is_action_pressed('dash'):
		velocity.x += speed
		running_right = false
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		running_left = true
	if running_left and Input.is_action_pressed('dash'):
		velocity.x -= speed
		running_left = false

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
